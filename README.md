# uMap-BAAC

Simple R script to process french BAAC files into uMap usable csv files.

Sample files provided are the 2022's batch.

Example output can be found at [https://umap.openstreetmap.fr/en/map/baac-2022-onisr_1002420](https://umap.openstreetmap.fr/en/map/baac-2022-onisr_1002420).
